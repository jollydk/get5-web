## How to:
1. Start the docker containers: 
```Bash
sudo docker-compose up -d --build
```

2. Run migrations:
```
sudo docker-compose exec web ./manager.py db upgrade
```

3. Everything should be running now.