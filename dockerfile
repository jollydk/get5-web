FROM ubuntu

RUN apt-get update && apt-get upgrade -y
RUN apt-get install git build-essential software-properties-common -y
RUN apt-get install python-dev python-pip apache2 libapache2-mod-wsgi -y
RUN apt-get install virtualenv libmysqlclient-dev -y

WORKDIR /var/www
RUN git clone https://github.com/splewis/get5-web

WORKDIR /var/www/get5-web
RUN virtualenv venv
RUN /bin/bash -c "source venv/bin/activate"
RUN pip install -r requirements.txt

COPY ./build/get5.wsgi /var/www/get5-web/get5.wsgi
COPY ./config/get5.conf /etc/apache2/sites-enabled/000-default.conf

RUN chown -R www-data:www-data /var/www/get5-web

CMD ["apachectl", "-D", "FOREGROUND"]